# Pause Control

Toggle pausing in various UI contexts. By default pausing is disabled for all UI modes that allow it.

The mod is configurable via the script settings menu:

ESC >> Options >> Scripts >> Pause Control

#### How It Works

There's some special handling for combat and dialogue:

* If the combat is initiated during dialogue, it's paused until dialogue is closed
* If the player enters dialgue while in combat with an actor, combat is paused until dialogue is closed

Additionaly: pausing for dialogue is force-enabled when a guard approaches you about committing a crime. This has to be done because if the game isn't paused, the guard will repeatedly prompt you to pay a fine (or otherwise).

There's an internal ignore list for actors using a model/animation that doesn't blend well with the feature that makes actors turn towards the player when dialogue is initiated. When a model/animation is added via an interface function, they won't turn when dialgue starts.

By default NPCs with the model `meshes/gvac/walllean.nif` are automatically added to this ignore list.

Below is an example Lua mod that adds some `.nif` files to the ignore list (this is a player script):

```lua
local PauseControl = require("openmw.interfaces").PauseControl

if not PauseControl then
  error("Pause Control is not installed!")
end

PauseControl.RegisterNoTurnModels({
  "meshes/some_mesh1.nif",
  "meshes/some_mesh2.nif",
})
```

This is an example of ignoring based on actor record ID:

```lua
local PauseControl = require("openmw.interfaces").PauseControl

if not PauseControl then
  error("Pause Control is not installed!")
end

PauseControl.RegisterNoTurnNPC({
  "some_actor_recordId1",
  "some_actor_recordId2",
})
```

#### Credits

Author: johnnyhostile

This mod was inspired by ptmikheev's excellent [UiModes](https://modding-openmw.gitlab.io/ui-modes/) and borrows some code from it.

#### Installation

##### With umo

1. Click the "Download with umo" button at the top of [this URL](https://modding-openmw.gitlab.io/pause-control/)
1. Run `umo cache sync custom`, then `umo install custom`

Adjust `custom` above if you're using a named custom list. Once installed:

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\UserInterface\PauseControl"`)
1. Add `content=pause-control.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

##### Manual

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/pause-control/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\UserInterface\PauseControl

        # Linux
        /home/username/games/UserInterface/PauseControl

        # macOS
        /Users/username/games/OpenMWMods/UserInterface/PauseControl

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\UserInterface\PauseControl"`)
1. Add `content=pause-control.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

#### How To Test This Mod

This mod has two special cases it handles, how to test them is described below.

##### Combat During Dialogue

1. Run the game
1. Press \` to bring down the console
1. Type `coe 0 -9` and press enter (note you may spawn inside of a rock!)
1. Walk east until you encounter the NPC Nels Llendo on the trail
1. Initiate dialogue, select the topic `Nels Llendo`, then `proposition`, then select `Never!`
1. Combat should have initiated but Nels will be standing there, waiting for dialogue to end before actually attacking

Another way to test this with multiple NPCs in combat with the player (not just the dialogue target):

1. Run the game
1. Press \` to bring down the console
1. Type `coc "Ilunibi, Soul's Rattle"` and press enter
1. Walk east until you hit a T in the cave, stay to the right as best as you can
1. A group of sleepers to your right will initiate combat, turn to your left to find Dagoth Gares and walk toward him
1. Dialogue will be initiated
1. Combat will be initiated with Gares but he and the attacking sleepers will halt combat until after dialogue has ended

##### Crime Dialogue With A Guard

1. Run the game
1. Go to any town
1. Punch or otherwise attack any NPC to commit a crime
1. Guards will chase after you
1. When a guard approaches you, dialogue will be initiated with the "You violated the law..." response
1. The game will be paused as this happens, and you will not see duplicate responses from the guard

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/pause-control/-/issues)
* Email `pause-control@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
