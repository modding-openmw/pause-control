## Pause Control Changelog

#### Version 1.7

* Handling for crimes has been improved:
    * If pause for dialogue is enabled, everything below is skipped
    * Instead of globally force-enabling pause for dialogue, the mod now checks for a nearby guard and force-enables pause only when they are almost close enough to initiate dialogue
    * If the player ends the situation peacefully, pause for dialogue is disabled
    * If the player has a crime level but is not close to a guard, pause for dialogue is disabled
* The "no turn" exception for `meshes/gvac/walllean.nif` is now loaded by default since several mods seem to use it
* Added a "How To Test This Mod" guide to the README that describes how to verify special handling of crime and combat during dialogue

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/36197723)

#### Version 1.6

* "No turn" exceptions are now done by model/animation for more broad, easy support (useful for the NPCs that are leaning against a wall from [Roaring Arena - Betting and Bloodletting](https://www.nexusmods.com/morrowind/mods/50954) as an example)

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/35486953)

#### Version 1.5

* Better hadling for the "no turn" exceptions
* Fixed a typo that broke the mod if you were using [Roaring Arena - Betting and Bloodletting](https://www.nexusmods.com/morrowind/mods/50954)

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/35167165)

#### Version 1.4

* Implemented a new method of handling combat initiated during dialogue that is content-agnostic
    * All instances of combat initiating during dialogue should be properly handled now
* All modes do not pause by default now
* Added an internal blacklist of NPCs that should not turn towards the player during dialogue
    * Notably, the "quest giver" NPC from GV's [Roaring Arena - Betting and Bloodletting](https://www.nexusmods.com/morrowind/mods/50954) shouldn't turn his whole body, just his head.

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/35094885)

#### Version 1.3

* Dialogue pausing is now forced when the player has a crime level in order to work around a potential dialogue bug

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/25127966)

#### Version 1.2

* NPCs will now stop and face the player during dialogue if pause is disabled

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/24985520)

#### Version 1.1

* Added pause controls for many more contexts:
    * Journal
    * Book
    * Scroll
    * Container
    * Alchemy
* Added missing localization files to the package

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/24453096)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/pause-control/-/packages/24288495)
