local self = require("openmw.self")
local types = require("openmw.types")
local util = require("openmw.util")
local I = require("openmw.interfaces")

local combatPaused = false
local noTurn
local target
local turn

local function dialogueStopped()
    target = nil
end

local function dialogueStarted(data)
    noTurn = data.noTurn
    target = data.player
end

local function onUpdate(deltaTime)
    if target then
        local pkg = I.AI.getActivePackage()
        if pkg then
            if pkg.type == "Combat" and pkg.target.type == types.Player and not combatPaused then
                print("[PauseControl]: Pausing AI for actor: " .. self.recordId)
                combatPaused = true
                self.enableAI(self, false)
            end
        end
        if noTurn then return end
        self.controls.movement = 0
        self.controls.sideMovement = 0
        local deltaPos = target.position - self.position
        local destVec = util.vector2(deltaPos.x, deltaPos.y):rotate(self.rotation:getYaw())
        local deltaYaw = math.atan2(destVec.x, destVec.y)
        if math.abs(deltaYaw) < math.rad(10) then
            turn = false
        elseif math.abs(deltaYaw) > math.rad(30) then
            turn = true
        end
        if turn then
            self.controls.yawChange = util.clamp(deltaYaw, -deltaTime * 2.5, deltaTime * 2.5)
        else
            self.controls.yawChange = 0
        end
    elseif combatPaused then
        print("[PauseControl]: Unpausing AI for actor: " .. self.recordId)
        self.enableAI(self, true)
        combatPaused = false
    end
end

return {
    engineHandlers = {onUpdate = onUpdate},
    eventHandlers = {
        momw_pc_dialogueStopped = dialogueStopped,
        momw_pc_dialogueStarted = dialogueStarted
    }
}
