local async = require("openmw.async")
local I = require("openmw.interfaces")
local common = require("scripts.pause-control.common")
local L = require("openmw.core").l10n(common.MOD_ID)
local storage = require("openmw.storage")
local modSettings = storage.playerSection("SettingsPlayer" .. common.MOD_ID)

I.Settings.registerPage {
    key = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "name",
    description = L("modDescription")
}

I.Settings.registerGroup {
    key = "SettingsPlayer" .. common.MOD_ID,
    l10n = common.MOD_ID,
    name = "name",
    page = common.MOD_ID,
    permanentStorage = false,
    settings = {
        {
            key = "pauseInventory",
            name = "pauseInventory_name",
            description = "pauseInventory_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseDialogue",
            name = "pauseDialogue_name",
            description = "pauseDialogue_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseJournal",
            name = "pauseJournal_name",
            description = "pauseJournal_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseBook",
            name = "pauseBook_name",
            description = "pauseBook_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseScroll",
            name = "pauseScroll_name",
            description = "pauseScroll_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseContainer",
            name = "pauseContainer_name",
            description = "pauseContainer_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "pauseAlchemy",
            name = "pauseAlchemy_name",
            description = "pauseAlchemy_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "debugMsgs",
            name = "debugMsgs_name",
            description = "debugMsgs_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local pauseInventory = modSettings:get("pauseInventory")
local pauseDialogue = modSettings:get("pauseDialogue")
local pauseJournal = modSettings:get("pauseJournal")
local pauseBook = modSettings:get("pauseBook")
local pauseScroll = modSettings:get("pauseScroll")
local pauseContainer = modSettings:get("pauseContainer")
local pauseAlchemy = modSettings:get("pauseAlchemy")
local debugMsgs = modSettings:get("debugMsgs")

local function updateSettings(_, key)
	if key == "pauseInventory" then
        pauseInventory = modSettings:get("pauseInventory")
    elseif key == "pauseDialogue" then
        pauseDialogue = modSettings:get("pauseDialogue")
    elseif key == "pauseJournal" then
        pauseJournal = modSettings:get("pauseJournal")
    elseif key == "pauseBook" then
        pauseBook = modSettings:get("pauseBook")
    elseif key == "pauseScroll" then
        pauseScroll = modSettings:get("pauseScroll")
    elseif key == "pauseContainer" then
        pauseContainer = modSettings:get("pauseContainer")
    elseif key == "pauseAlchemy" then
        pauseAlchemy = modSettings:get("pauseAlchemy")
    elseif key == "debugMsgs" then
        debugMsgs = modSettings:get("debugMsgs")
    end
end
modSettings:subscribe(async:callback(updateSettings))

return {
    pauseInventory = function() return pauseInventory end,
    pauseDialogue = function() return pauseDialogue end,
    pauseJournal = function() return pauseJournal end,
    pauseBook = function() return pauseBook end,
    pauseScroll = function() return pauseScroll end,
    pauseContainer = function() return pauseContainer end,
    pauseAlchemy = function() return pauseAlchemy end,
    debugMsgs = function() return debugMsgs end,
}
