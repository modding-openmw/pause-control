local async = require("openmw.async")
local nearby = require("openmw.nearby")
local self = require("openmw.self")
local storage = require("openmw.storage")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local common = require("scripts.pause-control.common")
local settings = require("scripts.pause-control.settings")
local modSettings = storage.playerSection("SettingsPlayer" .. common.MOD_ID)
local MODE = I.UI.MODE
local dialogueModes = {
    Barter = true,
    Companion = true,
    Dialogue = true,
    Enchanting = true,
    MerchantRepair = true,
    SpellBuying = true,
    SpellCreation = true,
    Training = true,
    Travel = true
}
local noTurnModels = {}
local noTurnNPCs = {}
local playerCrime = false

local function msg(...)
    if settings.debugMsgs() == true then
        print(string.format("[%s]: %s", common.MOD_ID, ...))
    end
end

local function registerNoTurnModels(paths)
    for _, path in pairs(paths) do
        msg(string.format("No turning for actors with the model: %s", path))
        noTurnModels[string.lower(path)] = true
    end
end

local function registerNoTurnNPC(actorRecordIds)
    for _, id in pairs(actorRecordIds) do
        msg(string.format("No turning for actor: %s", id))
        noTurnNPCs[string.lower(id)] = true
    end
end

local function modeUpdate()
    local dialogue = settings.pauseDialogue()
    local inventory = settings.pauseInventory()
    local journal = settings.pauseJournal()
    local book = settings.pauseBook()
    local scroll = settings.pauseScroll()
    local container = settings.pauseContainer()
    local alchemy = settings.pauseAlchemy()
    msg(string.format("Pause for dialogue: %s", dialogue))
    msg(string.format("Pause for inventory: %s", inventory))
    msg(string.format("Pause for journal: %s", journal))
    msg(string.format("Pause for book: %s", book))
    msg(string.format("Pause for scroll: %s", scroll))
    msg(string.format("Pause for container: %s", container))
    msg(string.format("Pause for alchemy: %s", alchemy))
    I.UI.setPauseOnMode(MODE.Interface, inventory)
    I.UI.setPauseOnMode(MODE.Journal, journal)
    I.UI.setPauseOnMode(MODE.Book, book)
    I.UI.setPauseOnMode(MODE.Scroll, scroll)
    I.UI.setPauseOnMode(MODE.Container, container)
    I.UI.setPauseOnMode(MODE.Alchemy, alchemy)
    for m in pairs(dialogueModes) do
        I.UI.setPauseOnMode(m, dialogue)
    end
end
modSettings:subscribe(async:callback(modeUpdate))
modeUpdate()

local function guardIsNear(actor)
    return actor.type == types.NPC
        and string.match(actor.type.record(actor).name, "Guard")
        and (actor.position - self.position):length() <= 200
end

local function onUpdate()
    if settings.pauseDialogue() then return end
    local crimeLevel = self.type.getCrimeLevel(self)
    if crimeLevel == 0 and not playerCrime then return end
    if crimeLevel > 0 and not playerCrime then
        local guardNear = false
        for _, actor in pairs(nearby.actors) do
            guardNear = guardIsNear(actor)
            if guardNear then break end
        end
        if guardNear then
            I.UI.setPauseOnMode("Dialogue", true)
            msg("Forcing dialogue pause because the player has a crime level and a guard is near!")
            playerCrime = true
        end
    elseif crimeLevel > 0 and playerCrime then
        local guardNear = false
        for _, actor in pairs(nearby.actors) do
            guardNear = guardIsNear(actor)
            if guardNear then break end
        end
        if not guardNear then
            msg("Restoring pause during dialogue preference because a guard is no longer near")
            modeUpdate()
            playerCrime = false
        end
    elseif crimeLevel == 0 and playerCrime then
        msg("Restoring pause during dialogue preference")
        modeUpdate()
        playerCrime = false
    end
end

local dialogueTarget = nil
local function UiModeChanged(data)
    if data.newMode == nil then
        if dialogueTarget then
            msg("Sending stopped dialogue event")
            dialogueTarget:sendEvent("momw_pc_dialogueStopped")
            dialogueTarget = nil
            for _, actor in pairs(nearby.actors) do
                actor:sendEvent("momw_pc_dialogueStopped")
            end
        end
    end
    if dialogueModes[data.newMode] and not settings.pauseDialogue()
        and data.arg and data.arg ~= dialogueTarget then
        if dialogueTarget then
            msg("Sending stopped dialogue event (with active newMode)")
            dialogueTarget:sendEvent("momw_pc_dialogueStopped")
        end
        msg("Sending started dialogue event")
        local t = data.arg
        local noTurn = noTurnModels[string.lower(t.type.record(t).model)]
            or noTurnNPCs[string.lower(t.recordId)]
        data.arg:sendEvent("momw_pc_dialogueStarted", {noTurn = noTurn, player = self})
        for _, actor in pairs(nearby.actors) do
            actor:sendEvent("momw_pc_dialogueStarted", {noTurn = noTurn, player = self})
        end
        dialogueTarget = t
    end
end

-- Several mods use this mesh
registerNoTurnModels({"meshes/gvac/walllean.nif"})

return {
    engineHandlers = {onUpdate = onUpdate},
    eventHandlers = {UiModeChanged = UiModeChanged},
    interfaceName = common.MOD_ID,
    interface = {
        version = 3,
        RegisterNoTurnModels = registerNoTurnModels,
        RegisterNoTurnNPC = registerNoTurnNPC
    }
}
