clean:
	cd $(proj_dir) && rm -f *.zip *.sha*sum.txt version.txt

pkg: clean
	cd $(proj_dir) && ./build.sh

web-all: web-clean web-build local-web

web-debug-all: web-clean web-debug local-web

web-clean:
	cd web && rm -rf build site/*.md sha256sums soupault-*-linux-x86_64.tar.gz

web-build: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

clean-all: clean web-clean

local-web:
	cd web && python3 -m http.server -d build
